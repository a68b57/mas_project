import java.awt.*;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.EtchedBorder;


//public class ProtPGPMM extends java.applet.Applet

public class MainActivity extends JFrame
{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;
	
	
	public static String mode;
	private JButton btnStart = new JButton("start");
	private JRadioButton rdbtnEve;
	private JRadioButton rdbtnD;
	private JRadioButton rdbtnM;
	private JRadioButton rdbtnUse;
	private JRadioButton rdbtnNotUse;
	
	public MainActivity() {
		
		super("MAS project");
		setBackground(UIManager.getColor("Button.highlight"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 600, 400);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setBackground(UIManager.getColor("Button.background"));
		
		init();
		setLocationRelativeTo(null);

			
	}
	

	
	
	public String chooseMode(){
		
		if(rdbtnEve.isSelected()&&rdbtnNotUse.isSelected()){
			return "eve";
		}
		if (rdbtnD.isSelected()&&rdbtnNotUse.isSelected()) {
			return "mikeD";
		}
		if (rdbtnM.isSelected()&&rdbtnNotUse.isSelected()) {
			return "mikeM";
		}
		if(rdbtnUse.isSelected()){
			return "PGP";
		}
		else return null;
	}
	
	
	
	public void init() {

		contentPane.setSize(600, 400);
		contentPane.setLayout(null);
		
		
		
		rdbtnEve = new JRadioButton("Eavesdropping",true);
		rdbtnEve.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
		rdbtnEve.setFocusPainted(true);
		rdbtnD = new JRadioButton("Interception",false);
		rdbtnD.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
		rdbtnM = new JRadioButton("Impersonation",false);
		rdbtnM.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));

		
		ButtonGroup attackMode = new ButtonGroup();
		attackMode.add(rdbtnEve);
		attackMode.add(rdbtnD);
		attackMode.add(rdbtnM);
		
		
		JPanel panelAttack = new JPanel();
		panelAttack.setBackground(UIManager.getColor("Button.background"));
		panelAttack.setLayout(new GridLayout(3, 1));
		panelAttack.add(rdbtnEve);
		panelAttack.add(rdbtnD);
		panelAttack.add(rdbtnM);
		panelAttack.setBounds(43, 37, 210, 214);
		TitledBorder titledBorderAttack = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Attack mode", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		titledBorderAttack.setTitleFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
		panelAttack.setBorder(titledBorderAttack);
		
		
		rdbtnUse = new JRadioButton("Yes",false);
		rdbtnUse.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));

		rdbtnNotUse = new JRadioButton("No",true);
		rdbtnNotUse.setFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
	
		ButtonGroup usePGP = new ButtonGroup();
		usePGP.add(rdbtnUse);
		usePGP.add(rdbtnNotUse);
				
		JPanel panelPGP = new JPanel();
		panelPGP.setBounds(316, 37, 210, 214);
		panelPGP.setLayout(new GridLayout(2, 1));
		panelPGP.add(rdbtnUse);
		panelPGP.add(rdbtnNotUse);
		TitledBorder titledBorderSec = new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Use PGP", TitledBorder.LEADING, TitledBorder.TOP, null, null);
		titledBorderSec.setTitleFont(new Font("Microsoft Sans Serif", Font.PLAIN, 20));
		panelPGP.setBorder(titledBorderSec);
		
		btnStart = new JButton("start");
		btnStart.setBackground(null);
		btnStart.setFont(new Font("Arial", Font.PLAIN, 20));
		btnStart.setBounds(403, 274, 123, 52);
		contentPane.add(btnStart);
		contentPane.add(panelPGP);
		contentPane.add(panelAttack);

		btnStart.addMouseListener(new MouseAdapter() {


			@Override
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				
				mode = chooseMode();
				ProtPGPMM protPGPMM = new ProtPGPMM(mode);
				System.out.println("mode from mainactivity: "+mode);
				protPGPMM.setVisible(true);
				//protPGPMM.setExtendedState(protPGPMM.getExtendedState() | JFrame.MAXIMIZED_BOTH);
				MainActivity.this.dispose();
				super.mouseClicked(e);
			}
		});
		
		
		
	}

	
	
	public  static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainActivity mainActivity = new MainActivity();
					mainActivity.setTitle("MAS project");
					mainActivity.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
					mainActivity.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
	}
}
