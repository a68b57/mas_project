public class Statement
{
	String sname;
	int state;

	public Statement(String paramString, int paramInt)
	{
		this.sname = paramString; // each of the steps of the protocol
		this.state = paramInt;  // state variable e.g. 100,110,120,130
	}

	public String getName() {
		return this.sname;
	}

	public int getState() {
		return this.state;
	}
}

