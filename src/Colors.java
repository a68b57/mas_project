import java.awt.Color;

public enum Colors {
    MIM_BACKGROUND(148, 148, 148),// darkgrey
    MIM_KB(188,188, 188), // lightgrey
    BOB_KB (172, 214, 255), // light blue
    BOB_BACKGROUND(108, 185, 251),// dark blue
//    BOB_BACKGROUND(28,145,188), // dark blue
    ALICE_KB(249,181,170), // light orange
    //ALICE_BACKGROUND(252,130,106), // dark orange
    ALICE_BACKGROUND(252,100,70), // dark orange new orange

    BLACK(0,0,0),
    WHITE(255,255,255),
    RED(225,89,106), // red
    ENCRYPTION_RED(234,0,0),//red for encrypted message
	LIGHTGREEN (58, 224, 166),
	DEFAULT_GREY (230,230,230);

    private final int r;
    private final int g;
    private final int b;
    private final String rgb;

    private Colors(final int r,final int g,final int b) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.rgb = r + ", " + g + ", " + b;
    }

    public String getRGB() {
        return rgb;
    }

    //You can add methods like this too
    public int getRed(){
        return r;
    }

    public int getGreen(){
        return g;
    }

    public int getBlue(){
        return r;
    }

    //Or even these
    public Color getColor(){
        return new Color(r,g,b);
    }

    public int getARGB(){
        return 0xFF000000 | ((r << 16) & 0x00FF0000) | ((g << 8) & 0x0000FF00) | b;
    }
}