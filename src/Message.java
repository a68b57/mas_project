

class Message
{
	String code;  // the confirm message sending at current state case, if processor is sending the message then code will be leeg
	int pos; //index of the code in the tape
	int from; //the person who send this message   0 is sender,1 is receiver, 2 is mike
	int socket; //the path in the map
	int lifeLeft;
	String mess = "9"; //the letter "real message"
	

	public Message()
	{
		this.code = "leeg";
		this.pos = 0;
		this.from = 0;
		this.lifeLeft = 3;
		this.socket = 0;
	}

	public String getCode()
	{
		return this.code;
	}

	public int getPos() {
		return this.pos;
	}

	public int getSocket() { return this.socket; }



	public int getFrom() {
		return this.from;
	}

	public int getLifeLeft() {
		return this.lifeLeft;
	}

	public String getMess() {
		return this.mess;
	}

	public void Timeflies(int paramInt)
	{
		this.lifeLeft -= paramInt;
	}

	public void setPos(int paramInt) {
		this.pos = paramInt;
	}

	public void setFrom(int paramInt) {
		this.from = paramInt;
	}

	public void setCode(String paramString) {
		this.code = paramString;
	}



	public void setMess(String paramString) {
		this.mess = paramString;
	}


	public Message copy(Message paramMessage) {
		paramMessage.code = this.code;
		paramMessage.pos = this.pos;
		paramMessage.from = this.from;
		paramMessage.mess = this.mess;
		paramMessage.lifeLeft = this.lifeLeft;
		return paramMessage;
	}
}


