import java.util.ArrayList;

class Tape
{
	ArrayList<String> convo = new ArrayList<String>();
	String s;

	
	public Tape(ArrayList<String> paramString)
	{
		convo = copyArrayList(paramString);
	}


	
	public String getCharAt(int paramInt)
	{
		return convo.get(paramInt);
	}


	public String getCharToStrAt(int paramInt) {
		return convo.get(paramInt);
	}

	
	public static ArrayList<String> copyArrayList(ArrayList<String> origin) {//a full copy to the argument arrayList
		ArrayList<String> copy = new ArrayList<String>();
		for(int i = 0; i<origin.size();i++){
			String temp = origin.get(i);
			copy.add(temp);
		}
		return copy;
	}
	
	
}






