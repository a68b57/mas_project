import java.util.Vector;

public class ProtocolAR extends Protocol {
	

	public ProtocolAR(String paramString) {
		this.name = paramString;
		makeProtocol();
	}

	public void makeProtocol()
	{
		Statement localStatement1 = new Statement("1 skip until Kr(Xi)", 160);
		Statement localStatement2 = new Statement("2 while true begin", 100);
		Statement localStatement3 = new Statement("3   write(Xi)", 120);
		Statement localStatement4 = new Statement("4   send(Kr(Xi)) until KrKsKr(Xi)", 130);
		Statement localStatement5 = new Statement("5   i:= i+1", 131);
		Statement localStatement6 = new Statement("6   send(KrKsKr(Xi)) until Kr(Xi+1)", 130);
		Statement localStatement7 = new Statement("7   i:= i+1", 140);
		Statement localStatement8 = new Statement("8 end", 150);

		this.statements = new Vector<Statement>();
		this.statements.addElement(localStatement1);
		this.statements.addElement(localStatement2);
		this.statements.addElement(localStatement3);
		this.statements.addElement(localStatement4);
		this.statements.addElement(localStatement5);
		this.statements.addElement(localStatement6);
		this.statements.addElement(localStatement7);
		this.statements.addElement(localStatement8);
	}

	public void msgControler(Message paramMessage, Processor paramProcessor)  // call this method only when a message is received
	{
		Message localMessage;
		if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 160) && (paramMessage.getCode().equals("leeg"))) 
			// paramMessage is the received message from other agent
			// paramProcessor.mess is the message that current processor holds
		{
			paramMessage.setFrom(1); // set the owner of the object "message" is receiver
			localMessage = new Message();
			localMessage = paramMessage.copy(localMessage); // copy paramMessage to local message
			paramProcessor.mess = localMessage;
			paramProcessor.mess.setCode("Kb");
			paramProcessor.skipuntil = true;
			paramProcessor.b = true;

		}
		else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("KaKb")) && (paramProcessor.mess.getCode().equals("Kb")))
		{
			paramMessage.setFrom(1);
			localMessage = new Message();
			
				localMessage = paramMessage.copy(localMessage);
				paramProcessor.mess = localMessage;
				paramProcessor.mess.setCode("KbKaKb");
				paramProcessor.senduntil = true;
				paramProcessor.b = true;
				
			
		}
		else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("leeg")) && (paramProcessor.mess.getCode().equals("KbKaKb")))
		{

			paramMessage.setFrom(1);
			localMessage = new Message();
			localMessage = paramMessage.copy(localMessage);
			paramProcessor.mess = localMessage;
			paramProcessor.mess.setCode("Kb");
			paramProcessor.senduntil = true;
			paramProcessor.b = true;
		}
	}
}