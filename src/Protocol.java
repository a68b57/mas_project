import java.util.Vector;

public abstract class Protocol
{
	public String name;
	protected Vector<Statement> statements;

	public abstract void makeProtocol();

	public Statement getStatement(int paramInt)
	{
		if (paramInt < this.statements.size())
			return (Statement)this.statements.elementAt(paramInt);
		return null;
	}

	public int getSize() {
		return this.statements.size();
	}

	public int getState() {
		return 0;
	}

	public void msgControler(Message paramMessage, Processor paramProcessor) {}
}


