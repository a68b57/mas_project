import java.util.Random;
import java.util.Vector;

public class Comline implements Runnable
{
	public Processor sender;
	public Processor receiver;
	public Processor mike;
	public MyScreen scherm;
	public double deletionFactor;
	public double slowFactor;
	Thread runner;
	int waittime;
	int time;
	Random r;
	double random;
	Vector<Message> underway;
	public static int fullLifeTime = 8500;
	

	public Comline(Processor paramProcessor1, Processor paramProcessor2, Processor mike, MyScreen paramMyScreen)
	{
		this.sender = paramProcessor1; // processor of the sender 
		this.receiver = paramProcessor2;
		this.mike = mike;
		this.scherm = paramMyScreen;   // the interface
		
		if (ProtPGPMM.mode == "eve" || ProtPGPMM.mode == "mikeD" || ProtPGPMM.mode == "mikeM") {
			this.waittime = 70;
		}else {
			this.waittime = 80;
		}
		  // waiting time between steps
		this.time = 50;
		this.underway = new Vector<Message>();
	}

	public void go() {
		if (this.runner == null) {
			this.runner = new Thread(this);
			this.runner.start();
 
		}// start the runner/thread
	}

	@SuppressWarnings("deprecation")
	public void halt() {
		if (this.runner != null) {
			this.runner.stop();
			this.runner = null;
		}
	}

	@SuppressWarnings("deprecation")
	public void stop() {
		if (this.runner != null) {
			this.runner.stop();
			this.runner = null;
			this.runner = new Thread();
		}
	}

	

	
	public void step() {
		this.sender.step(/*this.scherm.getAlgoPanel(0), */this.scherm.getTape(), this.scherm.getTapePanel(0), this); //0 is sender's panel

		//send send's algo panel, tape, tape panel and the whole thread to the 'step' method of the processor 

		this.mike.step(/*this.scherm.getAlgoPanel(2),*/ this.scherm.getTape(), this.scherm.getTapePanel(2), this);

		// scherm is myScreen
		this.receiver.step(/*this.scherm.getAlgoPanel(1), */this.scherm.getTape(), this.scherm.getTapePanel(1), this);
		
	}

	public void run()  //when press 'start'
	{
		for (;;)
		{
			step();   //keep doing the 'step'
			try {
				Thread.sleep(this.waittime);
			}
			catch (InterruptedException localInterruptedException) {}
		}
	}


	public void send(Message paramMessage)
	{
		
		
		this.r = new Random();
		this.random = this.r.nextDouble();
			
		
		if (ProtPGPMM.mode == "mikeD") {
			paramMessage.socket = 3;
			paramMessage.lifeLeft = fullLifeTime/2;// lifeLeft is how long the block will move before gone
		}
		else {
			paramMessage.socket = 2;
			paramMessage.lifeLeft = fullLifeTime;
		}

		addMesstoBuffer(paramMessage); // buffer of the system will get updated after the send by each of the processor
		UpdateUnderway();
		this.scherm.getCanvas().updateBuffer(getUnderwayBuffer());
	}

	public void UpdateUnderway() // update to put all the living messages into the underway buffer
	{
		if (!this.underway.isEmpty()) {  //underway is a vector
			int i = this.underway.size();
			Vector<Message>arrayOfMessage = new Vector<Message>();
			arrayOfMessage = copyVector(this.underway);//copy elements of underway into arrayOfMessage
			this.underway.removeAllElements(); // clear up the underway
			for (int j = 0; j < i; j++) { //i is the size of the underway
				Message localMessage = new Message();
				localMessage = arrayOfMessage.get(j);
				localMessage.Timeflies(getTime());
									
				
				if (localMessage.getLifeLeft() > 0) {
					this.underway.addElement(localMessage);// if the message is not died, still add it into underway. 
				} 
				
				if (ProtPGPMM.mode == "mikeM") { 
					if (localMessage.getLifeLeft() == fullLifeTime/2 ) {//TODO
						this.mike.setReceived(localMessage);// received by mike when it has 3000 life  (eve mode)
					}
				}
				
				
				if (ProtPGPMM.mode == "eve" || ProtPGPMM.mode == "PGP") { 
					if (localMessage.getLifeLeft() == fullLifeTime/2) { //TODO
						this.mike.setReceived(localMessage);					
					}
				}
				
				
				if(localMessage.getLifeLeft() == 0) {
					
					if (localMessage.getSocket() == 3 && localMessage.getFrom() == 2)  this.sender.setReceived(localMessage);
					if (localMessage.getSocket() == 3 && localMessage.getFrom() == 0)  this.mike.setReceived(localMessage);
					
					if (localMessage.getSocket() < 3) {
						if (localMessage.getFrom() == 0) {
							this.receiver.setReceived(localMessage); // where set the agent receive the message when lifeleft is < 0 
							
						} else {
							this.sender.setReceived(localMessage);
						}
					}
					this.sender.isDood();
				}
			}
		}
	}

	public void addMesstoBuffer(Message paramMessage) {// add message to the buffer(underway) if it doesn't exist before. doesn't add if it is there
		int i = 0;
		int j = this.underway.size();
		Vector<Message>arrayOfMessage = new Vector<Message>();

		arrayOfMessage = copyVector(this.underway);//copy elements of underway into arrayOfMessage

		for (int k = 0; k < j; k++) {
			//until it finds the message in the arrayOfMessage corresponding to paramMessage
			if ((paramMessage.getPos() == arrayOfMessage.get(k).getPos()) && (paramMessage.getCode().equals(arrayOfMessage.get(k).getCode())))
			{
				i = 1;
				k = j + 10; //10 is the total length of the conversation, in our case is 10
			}
		}
		if (i == 0) {//if the current message is not in the underway, add it
			this.underway.addElement(paramMessage);
		}
	}


	public int getTime()
	{
		return this.time;
	}

	public Vector<Message> getUnderwayBuffer() {
		return this.underway;
	}
	
	
	
	
	public static Vector<Message> copyVector(Vector<Message> origin) {//a full copy to the argument arrayList
		Vector<Message> copy = new Vector<Message>();
		for(int i = 0; i<origin.size();i++){
			Message temp = origin.get(i);
			copy.add(temp);
		}
		return copy;
	}
}


