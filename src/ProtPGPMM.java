import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.io.*;
import javax.imageio.ImageIO;
import javax.swing.*;



public class ProtPGPMM extends JFrame /*implements ActionListener*/
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	Screen screen;
	public static int posSen = 1000;
	public static TapePanel mikeTapePanel;
	public static String mode;
	public static int mRandom = 2;
	Image comBackground;
	private Button startbutton;
	private Button pausebutton;
	private Button stepbutton;
	private Button resetbutton;
	private Button home;

	Tape tape;
	Processor sender;
	Processor receiver;
	Processor mike;
	Comline comline;
	static ArrayList<String> convo;


	public ProtPGPMM(String mode) {

		super("MAS project");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(0, 0, 1500, 600);
		ProtPGPMM.mode = mode;
		System.out.println("mode in the program: "+mode);
		contentPane = new JPanel();
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout());

		init();
		pack();
		setLocationRelativeTo(null);
	}



	public void init() {


		convo = new ArrayList<String>();
		convo.add("go ahead");		
		convo.add("turn back");
		convo.add("turn left");
		convo.add("turn right");
		convo.add("hold on");
		convo.add("jump over");
		convo.add("keep down");
		convo.add("take stair");		
		convo.add("watch out");
		convo.add("go ahead");		
		convo.add("turn back");
		convo.add("turn left");
		convo.add("turn right");
		convo.add("hold on");
		convo.add("jump over");
		convo.add("keep down");
		convo.add("take stair");		
		convo.add("watch out");

		Collections.shuffle(convo);

		String srcPath = "/background/";


		try {
			if (mode == "PGP") {
				comBackground = ImageIO.read(getClass().getResource(srcPath+"ProtoA_PGP_new.png"));
			}
			if(mode != "PGP") comBackground = ImageIO.read(getClass().getResource(srcPath+"ProtoA_eve_new.png")); //if mode is PGP then change background
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		tape = new Tape(convo);
		sender = new Processor("sen", new ProtocolAS("AS"));
		receiver = new Processor("rec", new ProtocolAR("AR"));
		mike = new Processor("mim", new ProtocalAMIM("AMIM"));

		MyScreen localMyScreen = new MyScreen(comBackground, tape, sender, receiver, mike);

		screen = new Screen(localMyScreen); // create the layout of the interface
		comline = new Comline(sender, receiver, mike,localMyScreen); // create the object of the whole process

		JPanel localPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();


		Font font = new Font("Helvetica", Font.BOLD, 17);
		startbutton = new Button("Start");
		startbutton.setFont(font);
		c.weightx = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 0;
		c.ipady = 0;
		localPanel.add(startbutton,c);


		pausebutton = new Button("Pause");
		pausebutton.setFont(font);
		c.weightx = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 1;
		c.ipady = 0;
		localPanel.add(pausebutton,c);
		pausebutton.setEnabled(false);

		
		stepbutton = new Button("Step");
		stepbutton.setFont(font);
		c.weightx = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 2;
		c.ipady = 0;
		localPanel.add(stepbutton,c);


		resetbutton = new Button("Reset");
		resetbutton.setFont(font);
		c.weightx = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 3;
		c.ipady = 0;
		localPanel.add(resetbutton,c);


		home = new Button("Home");
		home.setFont(font);
		c.weightx = 10;
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipadx = 4;
		c.ipady = 0;
		localPanel.add(home,c);



		contentPane.add("North", screen);
		contentPane.add("South", localPanel);
		contentPane.setBackground(Color.WHITE);



	}


	public boolean action(java.awt.Event paramEvent, Object paramObject)
	{
		if ("Start".equals(paramObject)) {
			startbutton.setEnabled(false);
			pausebutton.setEnabled(true);	
			pausebutton.requestFocus();
			stepbutton.setEnabled(false);
			comline.go(); // where the process starts
			return true; }
		if ("Pause".equals(paramObject)) {
			startbutton.setEnabled(true);
			startbutton.requestFocus();
			pausebutton.setEnabled(false);
			stepbutton.setEnabled(true);
			comline.halt();
			return true; }
		if ("Step".equals(paramObject)) {
			startbutton.setEnabled(true);
			pausebutton.setEnabled(false);
			stepbutton.setEnabled(true);
			comline.step();
			return true;
		}

		if ("Reset".equals(paramObject)) {
			comline.stop();
			ProtPGPMM frame = new ProtPGPMM(mode);
			ProtPGPMM.this.dispose();
			frame.setVisible(true);
		}

		if ("Home".equals(paramObject)) {
			MainActivity frame = new MainActivity();
			comline.stop();
			ProtPGPMM.this.dispose();				
			frame.setVisible(true);
		}


		return false;
	}

}




