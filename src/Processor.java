
public class Processor
{
	Message mess;
	Message receiveMess;
	Protocol protocol;
	int lines;
	String statement;
	int prevRule;
	int nextRule;
	int pos;
	boolean senduntil;
	boolean skipuntil;
	int counter = 0;  
	int until = 12; // for wait until current state has repeat 7 time then do the actual task of this state
	int shortuntil = 12;
	Comline comline;
	boolean b;
	boolean dood = false;

	String name = "abravc";

	public Processor(String paramString, Protocol paramProtocol)
	{
		this.protocol = paramProtocol;  // indicates the protocol of the agent (confirms messages)
		this.lines = paramProtocol.getSize();
		this.name = paramString; // name of the owner of the protocol
		this.prevRule = 0;  //index of the state variable(100,110,120,130...) in the protocol 
		this.nextRule = 0;
		this.mess = new Message();
		if (paramString.equals("sen")){
			this.mess.setFrom(0); 
		}
		else if (paramString.equals("mim")) {
			this.mess.setFrom(2);
		}
		else {
				this.mess.setFrom(1);
			}
		this.receiveMess = null;
		this.pos = 0;
		this.senduntil = false;
		this.skipuntil = false;
	}

	public String getName()
	{
		return this.name;
	}

	public int getLines() {
		return this.lines;
	}

	
	public void step(/*AlgoPanel paramAlgoPanel,*/ Tape paramTape, TapePanel paramTapePanel, Comline paramComline)
	{// each processor run step method independently, 'mess' is not shared 		
		if (pos == 14) {
			paramComline.stop();
		}
		
		
		this.prevRule = this.nextRule; 
		this.comline = paramComline;
		if (this.b == true) {
			this.b = false;
		}

		Message localMessage;
				
		switch (getNextRule(this.nextRule)) { //get the case number 
		case 100:   //while 'true' begin , case 100 just wait until 7 
			if (this.counter == /*this.until*/this.shortuntil) {  // wait until counter is equal to 2
				setNextRule(this.nextRule + 1); //switch to the next index of the state of the protocol of the processor  
				//set the nextRule(index of the next state of the protocol) means read(Xi) (110 for sender, 120 for receiver)
				this.counter = 0;
			} else { this.counter += 1; }
			break;

		case 110:  // for sender, reads the message 
			if (this.counter == /*this.until*/this.shortuntil) { // hold on until counter to 2
				if (paramTape.getCharToStrAt(this.pos).equals("*")) {
					paramComline.stop();
				}
								
				paramTapePanel.updateTape(this.pos,mess.from);//highlight the position of the letter on the tape
				this.mess.setPos(this.pos); //set the position of the message regarding to the tape
				ProtPGPMM.posSen = this.pos;
				this.mess.setMess(paramTape.getCharToStrAt(this.pos)); //set the message to the 'mess'
				setNextRule(this.nextRule + 1); //go to the next state for sender (which is 130)
				this.counter = 0;
			} else { this.counter += 1; }
			break;

		case 120: 
			if (this.counter == /*this.until*/this.shortuntil) {//count until 2
				if (mess.from == 2) {
					if ((pos == ProtPGPMM.posSen)) {
						paramTapePanel.updateTape(pos,mess.from);
						setNextRule(this.nextRule + 1);//go to next state for receiver (which is 130)
						this.counter = 0;
					}
				}
				
				else{
				paramTapePanel.updateTape(this.pos,mess.from); //set the position of the letter according to the position on the message
				setNextRule(this.nextRule + 1);//go to next state for receiver (which is 130)
				this.counter = 0;
				}
			} else { this.counter += 1; }
			break;

			
			
		case 121: 
			if (this.counter == /*this.until*/this.shortuntil) {//count until 2
				if (mess.from == 2) {
					if ((pos == ProtPGPMM.posSen)) {
						paramTapePanel.updateTapeMIM(pos,mess.from);
						setNextRule(this.nextRule + 1);//go to next state for receiver (which is 130)
						this.counter = 0;
					}
				}
				
				else{
				paramTapePanel.updateTapeMIM(this.pos,mess.from); //set the position of the letter according to the position on the message
				setNextRule(this.nextRule + 1);//go to next state for receiver (which is 130)
				this.counter = 0;
				}
			} else { this.counter += 1; }
			break;
			
		case 130:  
				if (this.senduntil == true) { // sendunitl will be set to true when it is received by agent
					paramTapePanel.updateKb(this.pos, mess);
					setNextRule(this.nextRule + 1); // if anything sent from both agent are received by any agent(the second time 130), go to next state(140 for sender,140 for receiver) 
					this.senduntil = false;
				}
				else { // the mess of the processor is not setReceived
					localMessage = new Message();
					localMessage = this.mess.copy(localMessage);//copy the mess of the processor to localMessage
					if (ProtPGPMM.mode != "mikeD") {
						paramTapePanel.updateKbMIM(this.pos, mess, Comcanvas.mikeKUpdate);
					}
					paramComline.send(localMessage);  //call send method again if previous is not received
					
				}
			
			
			break;

		case 131: 
			if (this.counter == 40) {
				setNextRule(this.nextRule + 1);
				this.counter = 0;
			} else { this.counter += 1; }
			break;
		case 140: 
			if (this.counter == this.until) {
				this.pos += 1;
				setNextRule(this.nextRule + 1);
				this.counter = 0;
			} else { this.counter += 1; }
			break;
			
		case 170: 
			if (this.counter == this.until) {
				if(!(ProtPGPMM.posSen == pos)){
				pos = ProtPGPMM.posSen;
				setNextRule(this.nextRule + 1);}
				this.counter = 0;
			} else { this.counter += 1; }
			break;

		case 150: 
			if (this.counter == this.until) {
				if (this.name.equals("sen"))
					setNextRule(0); else
						setNextRule(1);
				this.counter = 0;
			} else { this.counter += 1; }
			break;

		case 160: 
			if (this.counter ==/* this.until*/this.shortuntil) { //count until 2
				if (this.skipuntil == true) { // skipuntil will only be true when the processor receives the message (this is updated by msgController)
					setNextRule(this.nextRule + 1); // next state is 100 for receiver, 100 for mike
					this.skipuntil = false;
				}
				this.counter = 0;
			} else { this.counter += 1; }
			break;

		}

	}

	
	
	public int getNextRule(int paramInt)  //paramInt is the index of the rule/ state variable 110,120,130... in the statement array of the processor protocol
	{
		return this.protocol.getStatement(paramInt).getState(); // return state variable
	}

	public void setNextRule(int paramInt) {
		this.nextRule = paramInt;
	}

	public void setReceived(Message paramMessage) {
		this.receiveMess = paramMessage; //receiveMess is just for update kcs
		this.protocol.msgControler(paramMessage, this); // once the message arrives at any agent, call msgControler method
	}

	public void isDood() {
		this.dood = true;
	}
}
