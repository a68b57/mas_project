import java.awt.Image;


class MyScreen
{
	Comcanvas cc;
	TapePanel[] panels = new TapePanel[3];
	Processor sender; Processor receiver; Processor mike; //processor man in middle
	
	Tape tape;


	public MyScreen(Image paramImage, Tape paramTape, Processor paramProcessor1, Processor paramProcessor2, Processor mike)
	{
		this.tape = paramTape;
		this.cc = new Comcanvas(paramImage, this);
		this.sender = paramProcessor1;
		this.receiver = paramProcessor2;
		this.mike = mike;

		this.panels[0] = new TapePanel(0, this.tape); //tape panel
		this.panels[1] = new TapePanel(1, this.tape);
		this.panels[2] = new TapePanel(2, this.tape);

	}

	public Comcanvas getCanvas()
	{
		return this.cc;
	}


	public TapePanel getTapePanel(int paramInt) {
		return this.panels[paramInt];
	}



	public Tape getTape() {
		return this.tape;
	}
}
