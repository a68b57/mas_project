import java.util.Vector;


public class ProtocalAMIM extends Protocol{

	public ProtocalAMIM(String paramString) {
		this.name = paramString;
		makeProtocol();
	}



	public void makeProtocol() {


		if (ProtPGPMM.mode == "eve" || ProtPGPMM.mode == "mikeM" || ProtPGPMM.mode == "PGP") {
			Statement localStatement1 = new Statement("1 skip until (Xi)", 160);
			Statement localStatement2 = new Statement("2 while true begin", 100);
			Statement localStatement3 = new Statement("3   write(Xi)", 121);
			Statement localStatement4 = new Statement("4   until(Xi+1)", 170);
			Statement localStatement5 = new Statement("5 end", 150);

			this.statements = new Vector<Statement>();
			this.statements.addElement(localStatement1);
			this.statements.addElement(localStatement2);
			this.statements.addElement(localStatement3);
			this.statements.addElement(localStatement4);
			this.statements.addElement(localStatement5);
		}else if (ProtPGPMM.mode == "mikeD") {
			Statement localStatement1 = new Statement("1 skip until Kr(Xi)", 160);
			Statement localStatement2 = new Statement("2 while true begin", 100);
			Statement localStatement3 = new Statement("3   write(Xi)", 120);
			Statement localStatement4 = new Statement("4   send(Kr(Xi)) until KrKsKr(Xi)", 130);
			Statement localStatement5 = new Statement("5   send(KrKsKr(Xi)) until Kr(Xi+1)", 130);
			Statement localStatement6 = new Statement("6   i:= i+1", 140);
			Statement localStatement7 = new Statement("7 end", 150);

			this.statements = new Vector<Statement>();
			this.statements.addElement(localStatement1);
			this.statements.addElement(localStatement2);
			this.statements.addElement(localStatement3);
			this.statements.addElement(localStatement4);
			this.statements.addElement(localStatement5);
			this.statements.addElement(localStatement6);
			this.statements.addElement(localStatement7);
		}

	}


	public void msgControler(Message paramMessage, Processor paramProcessor)
	{
		Message localMessage;

		switch (ProtPGPMM.mode) {
		case "eve":
			if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 160) && (paramMessage.getCode().equals("leeg")))
			{
				paramProcessor.skipuntil = true;
				paramProcessor.b = true;
			}
			break;

		case "PGP":
			if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 160) && (paramMessage.getCode().equals("leeg")))
			{
				paramProcessor.skipuntil = true;
				paramProcessor.b = true;
			}
			break;




		case "mikeD":
			if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 160) && (paramMessage.getCode().equals("leeg")))
			{
				paramMessage.setFrom(2); // set the owner of the object "message" is receiver
				localMessage = new Message();
				localMessage = paramMessage.copy(localMessage); // copy paramMessage to local message
				paramProcessor.mess = localMessage;
				paramProcessor.mess.setCode("Kb");
				paramProcessor.skipuntil = true;
				paramProcessor.b = true;

			}
			else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("KaKb")) && (paramProcessor.mess.getCode().equals("Kb")))
				//			else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("Ack2")) && (paramProcessor.mess.getCode().equals("Ack1")))
			{

				paramMessage.setFrom(2);
				localMessage = new Message();
				localMessage = paramMessage.copy(localMessage);
				paramProcessor.mess = localMessage;
				paramProcessor.mess.setCode("KbKaKb");
				paramProcessor.senduntil = true;
				paramProcessor.b = true;

			}
			else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("leeg")) && (paramProcessor.mess.getCode().equals("KbKaKb")))
				//			else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("leeg")) && (paramProcessor.mess.getCode().equals("Ack3")))
			{

				paramMessage.setFrom(2);
				localMessage = new Message();
				localMessage = paramMessage.copy(localMessage);
				paramProcessor.mess = localMessage;
				paramProcessor.mess.setCode("Kb");
				paramProcessor.senduntil = true;
				paramProcessor.b = true;
			}

			break;

		case "mikeM":
			if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 160) && (paramMessage.getCode().equals("leeg")))
			{
				paramProcessor.skipuntil = true;
				paramProcessor.b = true;
			}
			break;


		default:
			break;
		}





	}

}
