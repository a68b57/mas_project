import javax.swing.JPanel;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

class Screen extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel kbPanel;
	private JPanel gPanel;
	private String title;
	private String name;

	
	public Screen(MyScreen paramMyScreen)
	{		
		initialize(paramMyScreen);
	}

	public void initialize(MyScreen paramMyScreen)
	{
		
		switch (ProtPGPMM.mode) {
		case "eve":
			this.name = "Eve";
			this.title = "Eavesdropper";
			break;
		case "mikeD":
			this.name = "Mike";
			this.title = "Interception";
			break;
		
		case "PGP":
			this.name = "Mike";
			this.title = "Public key cryptography";
			break;
			
		default:
			this.name = "Mike";
			this.title = "Impersonation";
			break;
		}
		
		
		
		addAllComponents(paramMyScreen);
		
		GridBagConstraints c = new GridBagConstraints();
		setLayout(new GridBagLayout());
		setBackground(Color.white);
		
				
		java.awt.Label topWhiteUp = new java.awt.Label(title, 1); // the title
		topWhiteUp.setFont(new java.awt.Font("", 1, 30));
		topWhiteUp.setBackground(Colors.WHITE.getColor()); // white
		c.ipady = 30;
		c.gridx = 0;
		c.gridy = 1;
		add(topWhiteUp, c);
		
		
		
		c.fill = GridBagConstraints.HORIZONTAL;
		c.ipady = 160;      
		if (ProtPGPMM.mode == "PGP") {
			c.ipady = 325;
		}
		c.gridx = 0;
		c.gridy = 2;
		add(gPanel,c);
		
		
		java.awt.Label topWhiteDown = new java.awt.Label("",1);
		topWhiteUp.setFont(new java.awt.Font("", 1, 30));
		topWhiteDown.setBackground(Colors.WHITE.getColor()); // white
		c.ipady = 10;
		c.gridx = 0;
		c.gridy = 3;
		add(topWhiteDown, c);
		
				
		c.ipady = 0;
		c.gridx = 0;
		c.gridy = 4;
		add(kbPanel,c);
		
	}
	
	
	

	public void addAllComponents(MyScreen paramMyScreen) {
		
		
		
		
		//building gpanel
		
		BorderLayout gPanelLayout = new BorderLayout();
		gPanel = new JPanel(gPanelLayout);
		gPanel.add(paramMyScreen.getCanvas(),BorderLayout.CENTER);
		paramMyScreen.getCanvas().setOpaque(true);
		gPanel.setBackground(Color.BLACK);
		
		
		
		
		//building kb panel
		
		java.awt.Label labelRec = new java.awt.Label("Bob", 1);
		labelRec.setFont(new java.awt.Font("Bob", 1, 14));
		labelRec.setBackground(Colors.BOB_BACKGROUND.getColor());// dark blue


		java.awt.Label labelSen = new java.awt.Label("Alice", 1);
		labelSen.setFont(new Font("Alice", Font.BOLD, 14));
		labelSen.setBackground(Colors.ALICE_BACKGROUND.getColor()); // dark orange
		
		java.awt.Label labelMIM = new java.awt.Label(name, 1);
		labelMIM.setFont(new Font("Mike", Font.BOLD, 14));
		labelMIM.setBackground(Colors.MIM_BACKGROUND.getColor()); //dark grey
		
		JPanel kbPanelSen = new JPanel(new java.awt.BorderLayout());
		kbPanelSen.add("North", labelSen);
		kbPanelSen.add("Center",paramMyScreen.getTapePanel(0));
		
	    JPanel kbPanelRec = new JPanel(new java.awt.BorderLayout());
		kbPanelRec.add("North", labelRec);
		kbPanelRec.add("Center",paramMyScreen.getTapePanel(1));
	
		JPanel kbPanelMIM = new JPanel(new java.awt.BorderLayout());
		kbPanelMIM.add("North", labelMIM);
		kbPanelMIM.add("Center",paramMyScreen.getTapePanel(2));
		ProtPGPMM.mikeTapePanel = paramMyScreen.getTapePanel(2);
		

		kbPanel = new JPanel(new java.awt.BorderLayout()); //knowledge base panel
		kbPanel.add("West",kbPanelSen);
		kbPanel.add("Center",kbPanelMIM);
		kbPanel.add("East",kbPanelRec);

		
	}

	
	
}
