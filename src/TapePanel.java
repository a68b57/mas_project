import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Label;
import javax.swing.JPanel;

public class TapePanel extends JPanel
{


	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	Label[][] labels = null;

	int paramInt;
	Tape tape;
	int whereWeAre = 0;
	int slide = 0;
	GridBagConstraints gbc = new GridBagConstraints();


	public TapePanel(int paramInt, Tape paramTape) 
	{
		this.tape = paramTape;
		this.paramInt = paramInt;
		initialize(paramInt);

	}

	private void initialize(int paramInt) // paramInt here indicates sender(0) or receiver(1) 
	{
		Color grey = Colors.DEFAULT_GREY.getColor(); //default grey
		Color localColor1;
		Color red = Colors.RED.getColor(); // red


		if (paramInt == 2) { // mim
			labels = new Label[16][7];
			if (ProtPGPMM.mode == "mikeM") {
				labels = new Label[16][8];
			}
			localColor1 = Colors.MIM_KB.getColor();  // light grey paramInt = 2 (Mike)
		}else if (paramInt == 0) {
			labels = new Label[16][5];
			localColor1 = Colors.ALICE_KB.getColor();//light orange for alice
		}else{
			labels = new Label[16][5];
			localColor1 = Colors.BOB_KB.getColor(); // light blue for bob
		}


		setLayout(new GridBagLayout());


		Color[][] arrayOfColor = {
				{localColor1,localColor1,localColor1,localColor1,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,grey,red,red,localColor1},
				{localColor1,localColor1,localColor1,localColor1,localColor1},		
		};

		Color[][] arrayOfColorMIM = {
				{localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,grey,red,red,red,red,localColor1},
				{localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1},		
		};


		Color[][] arrayOfColorMIMmikeM = {
				{localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,grey,grey,red,red,red,red,localColor1},
				{localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1,localColor1},		
		};
		
		


		String[][] arrayOfString = { 	
				{ "Index", " Knowledge base "," k1"," k2"," "}, 
				{ "0"," "," "," "," "},
				{ "1"," "," "," "," "},
				{ "2"," "," "," "," "},
				{ "3"," "," "," "," "},
				{ "4"," "," "," "," "},
				{ "5"," "," "," "," "},
				{ "6"," "," "," "," "},
				{ "7"," "," "," "," "},
				{ "8"," "," "," "," "},
				{ "9"," "," "," "," "},
				{ "10"," "," "," "," "},
				{ "11"," "," "," "," "},
				{ "12"," "," "," "," "},
				{ "13"," "," "," "," "},
				{ " ", " "," "," "," "},
		};


		String[][] arrayOfStringMIM = { 	

				{ "Index", " Knowledge base "," k1"," k2"," "," "," "}, 				
				{ "0"," "," "," "," "," "," "},
				{ "1"," "," "," "," "," "," "},
				{ "2"," "," "," "," "," "," "},
				{ "3"," "," "," "," "," "," "},
				{ "4"," "," "," "," "," "," "},
				{ "5"," "," "," "," "," "," "},
				{ "6"," "," "," "," "," "," "},
				{ "7"," "," "," "," "," "," "},
				{ "8"," "," "," "," "," "," "},
				{ "9"," "," "," "," "," "," "},
				{ "10"," "," "," "," "," "," "},
				{ "11"," "," "," "," "," "," "},
				{ "12"," "," "," "," "," "," "},
				{ "13"," "," "," "," "," "," "},
				{ " ", " "," "," "," "," "," "},
		};

		
		String[][] arrayOfStringMIMmikeM = { 	

				{ "Index", "     True     ","   Malicious  "," k1"," k2"," "," "," "}, 				
				{ "0"," "," "," "," "," "," "," "},
				{ "1"," "," "," "," "," "," "," "},
				{ "2"," "," "," "," "," "," "," "},
				{ "3"," "," "," "," "," "," "," "},
				{ "4"," "," "," "," "," "," "," "},
				{ "5"," "," "," "," "," "," "," "},
				{ "6"," "," "," "," "," "," "," "},
				{ "7"," "," "," "," "," "," "," "},
				{ "8"," "," "," "," "," "," "," "},
				{ "9"," "," "," "," "," "," "," "},
				{ "10"," "," "," "," "," "," "," "},
				{ "11"," "," "," "," "," "," "," "},
				{ "12"," "," "," "," "," "," "," "},
				{ "13"," "," "," "," "," "," "," "},
				{ " ", " "," "," "," "," "," "," "},
		};
		
		
		
		


		if (paramInt == 2) { // mim
			if (ProtPGPMM.mode == "mikeM") {
				arrayOfStringMIMmikeM[0][3] = "   Kb   ";
				arrayOfStringMIMmikeM[0][4] = "  KaKb  ";
				arrayOfStringMIMmikeM[0][5] = " KbKaKb ";
				arrayOfStringMIMmikeM[0][6] = "KaKbKaKb";
			}
			arrayOfStringMIM[0][2] = "   Kb   ";
			arrayOfStringMIM[0][3] = "  KaKb  ";
			arrayOfStringMIM[0][4] = " KbKaKb ";
			arrayOfStringMIM[0][5] = "KaKbKaKb";
		}else if (paramInt == 0) {
			arrayOfString[0][2] = "   Kb   ";
			arrayOfString[0][3] = " KbKaKb ";
		}else{
			arrayOfString[0][2] = "  KaKb  ";
			arrayOfString[0][3] = "KaKbKaKb";
		}




		Font localFont = new Font("Helvetica", 0, 14);


		if (paramInt == 0 || paramInt == 1) {
			for (int i = 0; i < 16; i++) { // i is row, j is column
				for (int j = 0; j < 5; j++) {
					if (j == 0) {
						labels[i][j] = new Label(arrayOfString[i][0], 1);
						labels[i][j].setFont(localFont);
						gbc.gridx = j;
						gbc.gridy = i;
						gbc.fill = GridBagConstraints.HORIZONTAL;
						add(labels[i][j], gbc);
						getComponent(j+i*5).setBackground(arrayOfColor[i][j]);
					}else if (j == 4) {
						labels[i][j] = new Label(arrayOfString[i][4], 1);
						labels[i][j].setFont(localFont);
						gbc.gridx = j;
						gbc.gridy = i;
						gbc.fill = GridBagConstraints.HORIZONTAL;
						add(labels[i][j], gbc);
						getComponent(j+i*5).setBackground(arrayOfColor[i][j]);
					}else {
						labels[i][j] = new Label(arrayOfString[i][j], 1);
						labels[i][j].setFont(localFont);
						gbc.gridx = j;
						gbc.gridy = i;
						gbc.fill = GridBagConstraints.HORIZONTAL;
						add(labels[i][j], gbc);
						getComponent(j+i*5).setBackground(arrayOfColor[i][j]);
					}
				}
			}
		}else{ // tapePanel for mim
			
			if (ProtPGPMM.mode == "mikeM") { // mikeM mode
				for (int i = 0; i < 16; i++) { // i is row, j is column
					for (int j = 0; j < 8; j++) {
						if (j == 0) {
							labels[i][j] = new Label(arrayOfStringMIMmikeM[i][0], 1);
							labels[i][j].setFont(localFont);
							gbc.gridx = j;
							gbc.gridy = i;
							gbc.fill = GridBagConstraints.HORIZONTAL;
							add(labels[i][j], gbc);
							getComponent(j+i*8).setBackground(arrayOfColorMIMmikeM[i][j]);
						}else if (j == 7) {
							labels[i][j] = new Label(arrayOfStringMIMmikeM[i][7], 1);
							labels[i][j].setFont(localFont);
							gbc.gridx = j;
							gbc.gridy = i;
							gbc.fill = GridBagConstraints.HORIZONTAL;
							add(labels[i][j], gbc);
							getComponent(j+i*8).setBackground(arrayOfColorMIMmikeM[i][j]);
						}else {
							labels[i][j] = new Label(arrayOfStringMIMmikeM[i][j], 1);
							labels[i][j].setFont(localFont);
							gbc.gridx = j;
							gbc.gridy = i;
							gbc.fill = GridBagConstraints.HORIZONTAL;
							add(labels[i][j], gbc);
							getComponent(j+i*8).setBackground(arrayOfColorMIMmikeM[i][j]);
						}
					}
				}
			}
			
			else{ // other modes
					for (int i = 0; i < 16; i++) { // i is row, j is column
						for (int j = 0; j < 7; j++) {
							if (j == 0) {
								labels[i][j] = new Label(arrayOfStringMIM[i][0], 1);
								labels[i][j].setFont(localFont);
								gbc.gridx = j;
								gbc.gridy = i;
								gbc.fill = GridBagConstraints.HORIZONTAL;
								add(labels[i][j], gbc);
								getComponent(j+i*7).setBackground(arrayOfColorMIM[i][j]);
							}else if (j == 6) {
								labels[i][j] = new Label(arrayOfStringMIM[i][6], 1);
								labels[i][j].setFont(localFont);
								gbc.gridx = j;
								gbc.gridy = i;
								gbc.fill = GridBagConstraints.HORIZONTAL;
								add(labels[i][j], gbc);
								getComponent(j+i*7).setBackground(arrayOfColorMIM[i][j]);
							}else {
								labels[i][j] = new Label(arrayOfStringMIM[i][j], 1);
								labels[i][j].setFont(localFont);
								gbc.gridx = j;
								gbc.gridy = i;
								gbc.fill = GridBagConstraints.HORIZONTAL;
								add(labels[i][j], gbc);
								getComponent(j+i*7).setBackground(arrayOfColorMIM[i][j]);
							}
						}
					}
			
			}
			
			
		}
	}

	public void setLabelColorb(int row, int col, Color paramColor)  // paramInt1 is row, paramInt2 is column
	{
		int i = row*5 + col; 
		if (paramInt == 2) {
			if (ProtPGPMM.mode == "mikeM") {
				i = row*8+col;
			}else{
				i = row*7+col;
			}
		}
		getComponent(i).setBackground(paramColor);
	}

	public void setLabelColorf(int row, int col, Color paramColor) { // change the foreground
		int i = col + row * 5;
		if (paramInt == 2) {
			if (ProtPGPMM.mode == "mikeM") {
				i = row*8+col;
			}else{
			i = row*7+col;
			}
		}
		getComponent(i).setForeground(paramColor);
	}

	public void setMIMLabelColor(int row, int col, Color paramColor){
		if (ProtPGPMM.mode != "PGP" && ProtPGPMM.mode != "mikeM") {
			int i = col + row * 7;
			ProtPGPMM.mikeTapePanel.getComponent(i).setBackground(paramColor);
		}else if (ProtPGPMM.mode == "mikeM") {
			int i = col + row * 8;
			ProtPGPMM.mikeTapePanel.getComponent(i).setBackground(paramColor);
		}
	}



	public void updateTape(int paramInt, int messFrom) //paramInt is the position of the letter on the tape
	{
		for (int i = whereWeAre; i <= paramInt; i++) {
			String recText;
			if (ProtPGPMM.mode == "mikeM" && messFrom == 1) {
				recText = tape.getCharToStrAt(i+ProtPGPMM.mRandom);
			}else  recText = tape.getCharToStrAt(i);
			this.labels[(i - this.whereWeAre+1)][1].setText(recText);
		}

		int rowRed = 1 + paramInt - this.whereWeAre;
		int rowBlack = rowRed -1;
		setLabelColorf(rowRed, 0,Colors.RED.getColor());
		setLabelColorf(rowBlack, 0,Colors.BLACK.getColor());
		this.slide += 1;
	}

	
	public void updateTapeMIM(int paramInt, int messFrom) //paramInt is the position of the letter on the tape
	{
		for (int i = whereWeAre; i <= paramInt; i++) {
			String recText;
			if (ProtPGPMM.mode == "mikeM" && messFrom == 1) {
				recText = tape.getCharToStrAt(i+ProtPGPMM.mRandom);
			}
			else  {
				recText = tape.getCharToStrAt(i);
			}
			
			if (ProtPGPMM.mode == "PGP") {
				this.labels[(i - this.whereWeAre+1)][1].setText("Encrypted");
				this.labels[(i - this.whereWeAre+1)][1].setForeground(Colors.RED.getColor());
			}
			if (ProtPGPMM.mode == "mikeM") {
				this.labels[(i - this.whereWeAre+1)][1].setText(recText);
				this.labels[(i - this.whereWeAre+1)][2].setText(tape.getCharToStrAt(i+ProtPGPMM.mRandom));
			}
			else if (ProtPGPMM.mode == "eve" || ProtPGPMM.mode == "mikeD") {
				this.labels[(i - this.whereWeAre+1)][1].setText(recText);
			}
			
		}

		int rowRed = 1 + paramInt - this.whereWeAre;
		int rowBlack = rowRed -1;
		setLabelColorf(rowRed, 0,Colors.RED.getColor());
		setLabelColorf(rowBlack, 0,Colors.BLACK.getColor());
		this.slide += 1;
	}


	public void updateKb(int paramInt, Message mess){ // show the green stuff in kb for alice and Bob
		
			Color lightGreen = Colors.LIGHTGREEN.getColor();
			int rowGreen = 1 + paramInt - this.whereWeAre;
			if (mess.getCode().equals("KaKb")) { // set "Kb" green
				setLabelColorb(rowGreen, 2,lightGreen);
				if (ProtPGPMM.mode == "mikeD") setMIMLabelColor(rowGreen, 2, lightGreen);
			}else if(mess.getCode().equals("Kb")){ // set "KaKbKaKb" green
				setLabelColorb(rowGreen, 3,lightGreen);
				if (ProtPGPMM.mode == "mikeD") setMIMLabelColor(rowGreen, 5, lightGreen);
			}else if (mess.getCode().equals("KbKaKb")) { // set "KaKb" green
				setLabelColorb(rowGreen, 2,lightGreen);
				if (ProtPGPMM.mode == "mikeD") setMIMLabelColor(rowGreen, 3, lightGreen);
			}else if (mess.getCode().equals("leeg")) { // set "KbKaKb" green
				setLabelColorb(rowGreen, 3,lightGreen);
				if (ProtPGPMM.mode == "mikeD") setMIMLabelColor(rowGreen, 4, lightGreen);
			}
			
			this.slide += 1;	
	}
	
	public void updateKbMIM(int paramInt, Message mess, Boolean mikeKbupdate){ // paramInt is the pos, mess is the message
		// show the grenn stuff in kb for Mim
		if (mikeKbupdate == true) { 
			Color lightGreen = Colors.LIGHTGREEN.getColor();
			int rowGreen = 1 + paramInt - this.whereWeAre;
			if (mess.getCode().equals("Kb")) { // set "Kb" green
				if (ProtPGPMM.mode == "mikeM") {
					setMIMLabelColor(rowGreen, 3, lightGreen);
				}else setMIMLabelColor(rowGreen, 2, lightGreen);
			}else if(mess.getCode().equals("leeg")&&rowGreen-1 != 0){ // set "KaKbKaKb" green
				if (ProtPGPMM.mode == "mikeM") {
					setMIMLabelColor(rowGreen-1, 6, lightGreen);
				}else setMIMLabelColor(rowGreen-1, 5, lightGreen);
			}else if (mess.getCode().equals("KaKb")) { // set "KaKb" green
				if (ProtPGPMM.mode == "mikeM") {
					setMIMLabelColor(rowGreen, 4, lightGreen);
				}else setMIMLabelColor(rowGreen, 3, lightGreen);
			}else if (mess.getCode().equals("KbKaKb")) { // set "KbKaKb" green
				if (ProtPGPMM.mode == "mikeM") {
					setMIMLabelColor(rowGreen, 5, lightGreen);
				}else setMIMLabelColor(rowGreen, 4, lightGreen);
			}
			this.slide += 1;
		}
			
	}
	
	
}
