import java.util.Vector;

public class ProtocolAS extends Protocol {
	public ProtocolAS(String paramString) {
		this.name = paramString; //AS or RS
		makeProtocol();
	}


	public void makeProtocol()
	{
		Statement localStatement1 = new Statement("1 while true begin", 100);
		Statement localStatement2 = new Statement("2   read(Xi)", 110);
		Statement localStatement3 = new Statement("3   send(Xi) until KsKr(Xi)", 130);
		Statement localStatement4 = new Statement("4   i:= i+1", 131);
		Statement localStatement5 = new Statement("5   send(KsKr(Xi)) until KsKrKsKr(Xi)", 130);
		Statement localStatement6 = new Statement("6   i:= i+1", 140);
		Statement localStatement7 = new Statement("7 end", 150);

		this.statements = new Vector<Statement>();
		this.statements.addElement(localStatement1);
		this.statements.addElement(localStatement2);
		this.statements.addElement(localStatement3);
		this.statements.addElement(localStatement4);
		this.statements.addElement(localStatement5);
		this.statements.addElement(localStatement6);
		this.statements.addElement(localStatement7);

	}

	public void msgControler(Message paramMessage, Processor paramProcessor)
	{
		Message localMessage;
		if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("Kb")) && (paramProcessor.mess.getCode().equals("leeg")))

		{
			paramMessage.setFrom(0);
			localMessage = new Message();
			localMessage = paramMessage.copy(localMessage);
			paramProcessor.mess = localMessage;
			paramProcessor.mess.setCode("KaKb");
			paramProcessor.senduntil = true;
			paramProcessor.b = true;
		}
		else if ((paramProcessor.getNextRule(paramProcessor.nextRule) == 130) && (paramMessage.getCode().equals("KbKaKb")) && (paramProcessor.mess.getCode().equals("KaKb")))
		{
			paramMessage.setFrom(0);
			localMessage = new Message();
			localMessage = paramMessage.copy(localMessage);
			paramProcessor.mess = localMessage;
			paramProcessor.mess.setCode("leeg");
			paramProcessor.senduntil = true;
			paramProcessor.b = true;
		}
	}
}


