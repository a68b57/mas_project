import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;
import java.util.Vector;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

public class Comcanvas extends JPanel
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Graphics offScrGraphics;
	private java.awt.Image offScrImage;
	private java.awt.Image background;
	MyScreen scherm;
	static boolean mikeKUpdate = false;
	Vector<Message> buffer = new Vector<Message>();
	int pw = 150; // message block width
	int ph = 40;  // message block height
	int pwd2 = Math.round(this.pw / 2); //half of the block width
	int phd2 = Math.round(this.ph / 2); //half of the block height
	int canvasShifting = 0; 
	int canvasShiftingVertical = 0; // PGP set to -100, normal is 0;
	int messageStartShift = 100; // the shift of the position where the message shows up
	int lockSize = 40; // the size of the lock(squared)
	int xLock; //the x-position of the lock respecting to the message block, default is messBoxLen -100 
	int ylock = 45; // 30 lift up by 30
	int ySign = -7;
	int sizeSign = 25;
	int canvasSize = 1000;
	int messBoxLen = 0;
	String srcPath = "/lock/"; //path to the lock image
	String srcPathHLKey = "/key/"; //path to the lock image
	String srcSignPath = "/sign/";
	String srcSignBackPath = "/sign/";
	Image highlightKey;
	Image lockImageBob;
	Image lockImageAlice;
	Image signImageAlice;
	Image signImageBob;
	Image signBackgroundAlice;
	Image signBackgroundBob;
	int fullLifeTime = Comline.fullLifeTime;
	int xString = 70; // how much the beginning of the string moving to the left from the right border of the mess box, default is 65
	int yString = 5; // how much the string is move down respect to the upper border of the message box, default is 5
	Font encrypFont = new Font("Helvetica", Font.BOLD, 17);
	Font messageFont = new Font("Helvetica", Font.BOLD, 17);
	int messBoxScaleFactor = 10; // the length of the mess box is proprotional to the length of the string

	public Comcanvas(java.awt.Image paramImage, MyScreen paramMyScreen)
	{
		this.scherm = paramMyScreen;
		this.background = paramImage;
		setBackground(Color.white);
		try {
			this.lockImageBob = ImageIO.read(getClass().getResource(srcPath+"E_ALICE.png"));
			this.lockImageAlice = ImageIO.read(getClass().getResource(srcPath+"E_BOB.png"));
			this.highlightKey = ImageIO.read(getClass().getResourceAsStream(srcPathHLKey+"key.png"));
			this.signImageAlice = ImageIO.read(getClass().getResourceAsStream(srcSignPath+"S_ALICE.png"));
			this.signImageBob = ImageIO.read(getClass().getResourceAsStream(srcSignPath+"S_BOB.png"));
			this.signBackgroundAlice = ImageIO.read(getClass().getResourceAsStream(srcSignBackPath+"S_ALICE_BLACK.png"));
			this.signBackgroundBob = ImageIO.read(getClass().getResourceAsStream(srcSignBackPath+"S_BOB_BLACK.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		if (ProtPGPMM.mode == "mikeM") {
			canvasShifting = 25;
		}
		
		
		if (ProtPGPMM.mode == "PGP") {
			this.canvasShiftingVertical = -100;// put the canvas down for 100
			this.messBoxScaleFactor = 10;
		}else {
			this.canvasShiftingVertical = 0;
			this.messBoxScaleFactor = 10;
		}



	}


	public void paint(Graphics paramGraphics)
	{

		//		super.paint(paramGraphics);
		if (this.offScrImage == null) {
			Dimension localDimension = getSize();
			this.offScrGraphics = (this.offScrImage = createImage(localDimension.width, localDimension.height)).getGraphics();
		}

		xpaint(this.offScrGraphics);
		paramGraphics.drawImage(this.offScrImage, 0, 0, this);//60 px below the top label
	}



	public void update(Graphics paramGraphics) {
		paint(paramGraphics);
	}

	public void xpaint(Graphics paramGraphics) { // draw the background
		paramGraphics.setColor(Color.white);
		paramGraphics.fillRect(0,0,canvasShifting,background.getHeight(null));
		paramGraphics.fillRect(canvasShifting+background.getWidth(null),0,canvasShifting,background.getHeight(null));
		paramGraphics.drawImage(this.background, canvasShifting, 0, this);
		if (!this.buffer.isEmpty()) {
			drawPackets(paramGraphics);
		}
	}

	public void updateBuffer(Vector<Message> paramVector) {
		this.buffer = paramVector; // buffer is the vector, buffer here is the underway in Comline which contains all the living mess
		repaint();
	}


	private void drawPackets(Graphics paramGraphics) {

		if (this.buffer != null) {
			for (int i = 0; i < this.buffer.size(); i++) {
				Message localMessage = (Message)this.buffer.elementAt(i); //update each of the mess in the buffer
				int j = LifeTimeToHowFar(localMessage); // the relative position the message block start moving
				int k = HowFarToX(localMessage, j);  // absolute position the message block start moving
				int m = HowFarToY(localMessage, j);
				String str = " ";

				int n = localMessage.getPos();

				if (!localMessage.getCode().equals("leeg")) { // set the string
					str = localMessage.getCode(); //Acki
					if(ProtPGPMM.mode == "mikeM"){ 

						if (localMessage.getFrom()==0) { // TODO
							if(localMessage.getLifeLeft()<=fullLifeTime/2){
								str = str.concat("(" + new String(new StringBuffer().append(this.scherm.getTape().getCharToStrAt(n+ProtPGPMM.mRandom)).append(")").toString()));
							}else{
								str = str.concat("(" + new String(new StringBuffer().append(this.scherm.getTape().getCharToStrAt(n)).append(")").toString()));
							}
						} 

						if (localMessage.getFrom()==1) { //TODO
							if(localMessage.getLifeLeft()>=fullLifeTime/2){
								str = str.concat("(" + new String(new StringBuffer().append(this.scherm.getTape().getCharToStrAt(n+ProtPGPMM.mRandom)).append(")").toString()));
							}else{
								str = str.concat("(" + new String(new StringBuffer().append(this.scherm.getTape().getCharToStrAt(n)).append(")").toString()));
							}
						} 


					}else str = str.concat("(" + new String(new StringBuffer().append(this.scherm.getTape().getCharToStrAt(n)).append(")").toString()));


				} else {

					if (ProtPGPMM.mode == "mikeM" && localMessage.getLifeLeft()<=fullLifeTime/2) { //TODO 
						str = new String(this.scherm.getTape().getCharToStrAt(n+ProtPGPMM.mRandom));
						paramGraphics.setColor(Colors.MIM_KB.getColor()); //light grey
					}else  str = new String(this.scherm.getTape().getCharToStrAt(n));
				}


				messBoxLen = str.length()*messBoxScaleFactor;


				// set the color and size of the graphic components
				if(k>0) {
					// set block color
					if (localMessage.getFrom() == 0) {
						if (localMessage.getLifeLeft()<fullLifeTime/2 && ProtPGPMM.mode == "mikeM" ) {
							paramGraphics.setColor(Colors.MIM_KB.getColor()); //light grey
							mikeKUpdate = true;
						}else if (localMessage.getLifeLeft() == fullLifeTime/2 && ProtPGPMM.mode == "mikeM") {
							// kb for MIM update
							mikeKUpdate = true;
						}
						else if(ProtPGPMM.mode != "mikeD"){
							paramGraphics.setColor(Colors.ALICE_KB.getColor()); //light orange
							if (localMessage.getLifeLeft()<fullLifeTime/2) {
								mikeKUpdate = true;
							}else{
								mikeKUpdate = false;
							}
						}else {
							paramGraphics.setColor(Colors.ALICE_KB.getColor());
						}
					}else if (localMessage.getFrom() == 1) {

						if (localMessage.getLifeLeft()<fullLifeTime/2 && ProtPGPMM.mode == "mikeM") {
							paramGraphics.setColor(Colors.MIM_KB.getColor()); //light grey
							mikeKUpdate = true;
						}else if (localMessage.getLifeLeft() == fullLifeTime/2 && ProtPGPMM.mode == "mikeM") {
							// kb for MIM update
							mikeKUpdate = true;
						}
												
						else if(ProtPGPMM.mode != "mikeD"){// other modes
							paramGraphics.setColor(Colors.BOB_KB.getColor()); // light blue
							if (localMessage.getLifeLeft()<fullLifeTime/2) {
								mikeKUpdate = true;
							}else{
								mikeKUpdate = false;
							}
						}
						else {
							paramGraphics.setColor(Colors.ALICE_KB.getColor());
						}
					}else if (localMessage.getFrom() == 2) {
						paramGraphics.setColor(Colors.MIM_KB.getColor()); //light grey


					}
					
					
					
				
					paramGraphics.setFont(messageFont);

					
					
					// draw the lock ,string, highlight key and sign

					if (ProtPGPMM.mode == "PGP") {

						//xLock = messBoxLen-100;

						int remainLife = localMessage.getLifeLeft();
						int temp = fullLifeTime/5;

						switch (localMessage.getFrom()){
						case 0: // message from alice to bob
							drawAllComponents(remainLife, temp, paramGraphics, k, str, m, "Alice");
							break;

						case 1: // message from bob to alice
							drawAllComponents(remainLife, temp, paramGraphics, k, str, m, "Bob");
							break;
						default:
							break;
						}
					}

					else {
						messBoxLen = str.length() * messBoxScaleFactor;
						// draw the block 
						paramGraphics.fillRect(k - this.pwd2+canvasShifting, m - this.phd2-canvasShiftingVertical, messBoxLen, this.ph); // set the width and height and location of the message block
						paramGraphics.setColor(Colors.BLACK.getColor());// black
						paramGraphics.drawRect(k - this.pwd2+canvasShifting, m - this.phd2-canvasShiftingVertical, messBoxLen, this.ph); // set the width height and location of the message block

						// draw string
						paramGraphics.drawString(str, k -xString + canvasShifting, m + yString-canvasShiftingVertical); // the starting point of the string in the block
					}


				}
			}

		}
	}




	private void drawNormalMessBoxStr(Graphics paramGraphics, int k, String str, int m, boolean encrypted) {
		int messBoxLen = str.length()*messBoxScaleFactor;
		// draw the block 
		paramGraphics.fillRect(k - this.pwd2+canvasShifting, m - this.phd2-canvasShiftingVertical, messBoxLen, this.ph); // set the width and height and location of the message block
		paramGraphics.setColor(Colors.BLACK.getColor());// black
		paramGraphics.drawRect(k - this.pwd2+canvasShifting, m - this.phd2-canvasShiftingVertical, messBoxLen, this.ph); // set the width height and location of the message block

		if (encrypted) {

			paramGraphics.setColor(Colors.ENCRYPTION_RED.getColor());//red
			paramGraphics.setFont(encrypFont);
			paramGraphics.drawString(str, k -xString + canvasShifting, m + yString-canvasShiftingVertical); // the starting point of the string in the block
		}
		// draw string
		paramGraphics.drawString(str, k -xString + canvasShifting, m + yString-canvasShiftingVertical); // the starting point of the string in the block
	}


	private void drawSignImage(Graphics paramGraphics, int k, String str, int m, String name) {
		int xLock = str.length()*messBoxScaleFactor-100;
		if (name == "Alice") {
			paramGraphics.drawImage(signImageAlice, k - canvasShifting+xLock+12, m-canvasShiftingVertical-ySign, sizeSign, sizeSign,null);
		}else {
			paramGraphics.drawImage(signImageBob, k - canvasShifting+xLock+12, m-canvasShiftingVertical-ySign, sizeSign, sizeSign,null);
		}
		paramGraphics.drawRect(k - canvasShifting+xLock+12, m-canvasShiftingVertical-ySign, sizeSign, sizeSign);
	}


	private void highlightEncryKeyAlice(Graphics paramGraphics) {
		paramGraphics.drawImage(highlightKey, canvasShifting+377, 20, 47, 47, null);
	}

	private void highlightDecryKeyBob(Graphics paramGraphics) {
		paramGraphics.drawImage(highlightKey, canvasSize-canvasShifting-23, 45, 47, 47, null);
	}


	private void highlightDecryKeyAlice(Graphics paramGraphics) {
		paramGraphics.drawImage(highlightKey, canvasShifting+100, 238, 47, 47, null);
	}

	private void highlightEncryKeyBob(Graphics paramGraphics) {
		paramGraphics.drawImage(highlightKey, canvasSize-canvasShifting-308, 265, 47, 47, null);
	}


	private void drawSignBackground(Graphics paramGraphics, String name) {
		if (name == "Alice") {
			paramGraphics.drawImage(signBackgroundAlice, canvasShifting+297, 39, 47, 47, null);

		}else paramGraphics.drawImage(signBackgroundBob, canvasSize-canvasShifting-228, 245, 47, 47, null);

	}


	private void drawLockImage(Graphics paramGraphics,int k, String str, int m,String name) {
		int xLock = str.length()*messBoxScaleFactor-99;

		if (name == "Alice") {
			paramGraphics.drawImage(lockImageAlice, k - canvasShifting+xLock, m-canvasShiftingVertical-ylock, lockSize, lockSize,null);
		}else{
			paramGraphics.drawImage(lockImageBob, k - canvasShifting+xLock, m-canvasShiftingVertical-ylock, lockSize, lockSize,null);
		}
	}



	private void drawAllComponents(int remainLife, int temp, Graphics paramGraphics, int k, String str, int m, String name) {
		if (remainLife <=0.8*temp) { // only message block, sign image and string
			drawNormalMessBoxStr(paramGraphics,k,str,m,false);
			paramGraphics.setColor(Colors.RED.getColor()); // highlight the border of the sign image
			drawSignImage(paramGraphics, k, str,m,name);
		}

		if (remainLife>0.8*temp && remainLife <=1*temp) {  // draw mess box, string, sign image and highlight key
			drawNormalMessBoxStr(paramGraphics,k,str,m,false);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, str,m,name);

			if (name == "Alice") {
				highlightDecryKeyBob(paramGraphics);
			}else{
				highlightDecryKeyAlice(paramGraphics);
			}



		}
		if (remainLife>1*temp && remainLife <=1.5*temp) { // draw mess box, encrypted string, sign image, lock image and highlight key
			String eString = "Eb("+str+")";
			drawNormalMessBoxStr(paramGraphics,k,eString,m,true);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, eString,m,name);
			if (name == "Alice") {
				highlightDecryKeyBob(paramGraphics);
			}else{
				highlightDecryKeyAlice(paramGraphics);
			}
			drawLockImage(paramGraphics,k,eString,m,name);
		}
		if (remainLife>1.5*temp && remainLife <=2.8*temp) { // draw mess box, encrypted string, sign image, lock image
			String eString = "E("+str+")";
			drawNormalMessBoxStr(paramGraphics,k,eString,m,true);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, eString,m,name);
			drawLockImage(paramGraphics,k,eString,m,name);

		}
		if (remainLife>2.8*temp && remainLife <=3*temp) {// draw mess box, encrypted string, sign image, lock image and highlight key
			String eString = "E("+str+")";
			drawNormalMessBoxStr(paramGraphics,k,eString,m,true);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, eString,m,name);

			if (name == "Alice") {
				highlightEncryKeyAlice(paramGraphics);
			}else{
				highlightEncryKeyBob(paramGraphics);
			}


			drawLockImage(paramGraphics,k,eString,m,name);	



		}
		if (remainLife>3*temp && remainLife <=3.5*temp) {// draw mess box, encrypted string, sign image, and highlight key
			drawNormalMessBoxStr(paramGraphics,k,str,m,false);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, str,m,name);

			if (name == "Alice") {
				highlightEncryKeyAlice(paramGraphics);
			}else{
				highlightEncryKeyBob(paramGraphics);
			}

		}
		if (remainLife>3.5*temp && remainLife <=4.2*temp) {
			drawNormalMessBoxStr(paramGraphics,k,str,m,false);
			paramGraphics.setColor(Colors.BLACK.getColor());// black for the border of sign image
			drawSignImage(paramGraphics, k, str,m,name);
		}
		if (remainLife >4.2*temp ) {
			drawNormalMessBoxStr(paramGraphics,k,str,m,false);
			drawSignBackground(paramGraphics, name);
		}

	}



	private int LifeTimeToHowFar(Message paramMessage)
	{
		float f1 = 0.0F;
		float f2 = 0.0F;

		if (paramMessage.getSocket() == 2) {
			f1 = fullLifeTime;
			f2 = fullLifeTime/10;
		}
		if (paramMessage.getSocket() == 3) { 
			f1 = fullLifeTime/2;
			f2 = f1/10;
		}
		float f3 = f1 - paramMessage.getLifeLeft(); // the traveled distance
		float f4 = f3 / f1; // the proportion of the traveled distance
		float f5 = f4 * f2;
		int i = Math.round(f5);

		return i;
	}

	private int HowFarToX(Message paramMessage, int paramInt) {

		if (ProtPGPMM.mode == "eve" || ProtPGPMM.mode == "mikeM" || ProtPGPMM.mode == "PGP") {
			if (paramMessage.getSocket() == 2 && paramMessage.getFrom() == 1) {
				return canvasSize - paramInt;
			}
			if (paramMessage.getFrom() == 0) {
				return paramInt + messageStartShift;
			}


		}else if (ProtPGPMM.mode=="mikeD") {
			if (paramMessage.getSocket() == 3 && paramMessage.getFrom() == 0) {
				return  paramInt + messageStartShift;
			}

			if (paramMessage.getSocket() == 3 && paramMessage.getFrom() == 2) {
				return  canvasSize/2-(paramInt-messageStartShift);
			}

		}
		return 0;
	}





	private int HowFarToY(Message paramMessage, int paramInt)
	{
		int i = 50;

		if (paramMessage.getSocket() == 2 && paramMessage.from == 0) { i = 32; // socket 2 path for sender
		}
		if (paramMessage.getSocket() == 2 && paramMessage.from == 1) { i = 100; // socket 2 path for mike and receiver
		}
		if (paramMessage.getSocket() == 2 && paramMessage.from == 2) { i = 100; 
		}

		if (paramMessage.getSocket() == 3 && paramMessage.from == 0) { i = 32; // socket 2 is the middle path in the full network
		}
		if (paramMessage.getSocket() == 3 && paramMessage.from == 1) { i = 100; // socket 2 is the middle path in the full network
		}
		if (paramMessage.getSocket() == 3 && paramMessage.from == 2) { i = 100; // socket 2 is the middle path in the full network
		}
		return i;
	}
	
	
	
	
	
}




